#!/bin/bash
MY_DIR=$(cd $(dirname $0) && pwd)
PLATFORM_DIR=$(cd $(dirname $0) && cd ../.. && pwd)
TEGRA_PLATFORM_DIR=$(cd $PLATFORM_DIR && cd ../tegra && pwd)
SOC_DIR=$(cd $PLATFORM_DIR && cd ../../soc/t18x && pwd)
TEGRA_SOC_DIR=$(cd $SOC_DIR && cd ../tegra && pwd)
KERNEL_PARENT_DIR=$(cd $PLATFORM_DIR && cd ../../../../kernel && pwd)
CPP_INCLUDES=""
DTC_INCLUDES=""
INCDIRS="\
  $SOC_DIR/kernel-include \
  $TEGRA_SOC_DIR/kernel-include \
  $TEGRA_SOC_DIR/kernel-include/kernel-4.4 \
  $KERNEL_PARENT_DIR/kernel-4.4/include \
  $SOC_DIR/kernel-dts \
  $TEGRA_PLATFORM_DIR/common/kernel-dts \
  $PLATFORM_DIR/common/kernel-dts \
  $PLATFORM_DIR/quill/kernel-dts \
  $MY_DIR \
"
for incdir in $INCDIRS; do
    if [ -e $incdir ]; then
        CPP_INCLUDES="${CPP_INCLUDES} -I ${incdir}"
        DTC_INCLUDES="${DTC_INCLUDES} -i ${incdir}"
        echo "${incdir}"
    else
        echo "MISSING ${incdir}"
    fi
done
FORCEFLAGS="\
  -i $TEGRA_SOC_DIR/kernel-include/dt-bindings/display/tegra-dc.h
"
#echo "${CPP_INCLUDES//-I/}" | tr " " "\n"
cpp -E -nostdinc -undef -D__DTS__ -x assembler-with-cpp \
  ${CPP_INCLUDES} \
  $1 > $1.i && \
  dtc ${DTC_INCLUDES} -I dts -O dts --sort --force $1.i